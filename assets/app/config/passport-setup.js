const passport = require('passport');
const LocalStrategy = require('passport-local');
const bcrypt = require('bcrypt');
const winston = require('../../shared/winston');
const databaseFunctions = require('../../shared/databaseFunctions');

passport.serializeUser((user, done) => {
    winston.debug('inside function serializeUser');
    if (user.userId) {
        winston.debug(`serialize userId: ${user.userId}`);
        done(null, user.userId);
    } else {
        done(null, user);
    }
});

passport.deserializeUser((userId, done) => {
    databaseFunctions.selectTable('users', { userId: userId }).then((thisUser) => {
        if (thisUser.length == 1) {
            winston.sql(`got user mathing userId ${thisUser[0].userId}. Updating time last visited now`);
            databaseFunctions.updateTableRow('users', { userId: thisUser[0].userId }, { lastVisited: new Date() }).then(() => {
                done(null, thisUser[0]);
            }); //needs catch
        } else {
            winston.debug('calling done with params err and null');
            done(null, null);
        }
    });
});

// eslint-disable-next-line no-unused-vars
async function signInComplete(iss, sub, profile, accessToken, refreshToken, params, done) {
    //store the accesstoken and refreshToken un user token
    // console.log(accessToken);
    // called at sign in complete an not currently used, but can be over-ridden
    return done(null, accessToken);
}

passport.use(
    new LocalStrategy({
        //Local strategy options
        usernameField: 'emailAddress',
        passwordField: 'password'
    }, (username, password, done) => {
        //look to migrate into customsOfficer
        winston.debug('inside local strategy');

        databaseFunctions.selectTable('users', { 'emailAddress': username }).then((matchedUsers) => {
            winston.debug('selected table users....');
            console.log(matchedUsers);
            if (matchedUsers.length == 1) {
                const matchedUser = matchedUsers[0];
                bcrypt.compare(password, matchedUser.password).then((result) => {
                    winston.debug(`bcrypt result....${result}`);
                    if (result == true || password == matchedUser.password) {
                        done(null, matchedUser);
                    } else {
                        done(null, null);
                    }
                });
            } else {
                done(null, null);
            }
        });
    })
);