const express = require('express');
const router = express.Router();
const winston = require('../../shared/winston');
const passport = require('passport');
const fetch = require('node-fetch');
const settings = require('../../../scripts/settings');
const cred = require('../../../credentials/credentials');
const BaseError = require('../classes/BaseError');
const error = new BaseError('An error occured', { logLevel: 'error', displayType: 'standard', severity: 'normal' });
const { v4: uuidv4 } = require('uuid');
const modalError = new BaseError('An error occured', { logLevel: 'error', displayType: 'modal', severity: 'normal', containerName: 'baseErrorModalContent' });
const consoleError = new BaseError('An serious error occured', { logLevel: 'error', displayType: 'console', severity: 'high' });
const databaseFunctions = require('../../shared/databaseFunctions');


router.all('*', (req, res, next) => {
    //auth route catch all behaviour
    winston.info('In Auth Route....');
    next();
});

router.get('/login', (req, res, next) => {
    if (req.query.loginFailed == 'true') {
        error.raise('Invalid username or password.', req.url).push(res);
        modalError.raise('Something went wrong - check the errors on the page.', req.url).push(res);
        consoleError.raise('You can put some helpful debug info here if required.', req.url).push(res);
    }
    res.locals.successUrl = req.query.successUrl || '/';
    if (res.locals.extended) {
        next();
    } else {
        return res.render(`${res.locals.baseTemplate}/auth/login.ejs`);
    }
});

router.get('/failed', (req, res) => {
    //needs to be completed
    return res.send(`DONE ${req.originalUrl}`);
});

router.post('/email', (req, res, next) => {
    // eslint-disable-next-line no-unused-vars
    passport.authenticate('local', (err, user, info) => {
        if (err) {
            winston.error('Passport error:');
            winston.error(err);
            return res.redirect(`/auth/login?loginFailed=true&successUrl=${req.body.successUrl}`);
        }
        if (!user) {
            winston.error('User credentials failed');
            return res.redirect(`/auth/login?loginFailed=true&successUrl=${req.body.successUrl}`);
        }
        req.logIn(user, (err) => {
            if (err) { return res.redirect(`/auth/login?loginFailed=true&successUrl=${req.body.successUrl}`); }
            return res.redirect(req.body.successUrl);
        });
    })(req, res, next);
});

router.post('/loginIndexDb', (req, res, next) => {
    // eslint-disable-next-line no-unused-vars
    passport.authenticate('local', (err, user, info) => {
        if (err) {
            winston.error('Passport error:');
            winston.error(err);
            return res.json(error.raise(err, req.url));
        }
        if (!user) {
            winston.error('User credentials failed');
            return res.json(error.raise('Invalid Username or password', req.url));
        }
        req.logIn(user, (err) => {
            if (err) {
                return res.json(error.raise(err, req.url));
            }
            if (!req.session.passport.sessionId) {
                req.session.passport.sessionId = uuidv4();
            }
            return res.json({ sessionId: req.session.passport.sessionId });
        });
    })(req, res, next);
});


router.get('/clearSession', (req, res) => {
    if (req.session.passport) {
        req.session.passport.sessionId = false;
        req.logout();
    }
    return res.json({});
});


router.get('/setPassword', (req, res, next) => {
    res.locals.authToken = req.query.authToken || 'INVALID';
    if (res.locals.extended) {
        next();
    } else {
        return res.render(`${res.locals.baseTemplate}/auth/setPassword.ejs`);
    }
});

router.post('/setPassword', (req, res) => {
    const authToken = req.body.authToken || 'INVALID';
    const password = req.body.password || '';
    databaseFunctions.selectTable('users', { emailVerificationToken: authToken }).then((matchingUser) => {
        if (matchingUser.length == 1) {
            matchingUser = matchingUser[0];
            var payload = {
                emailAddress: matchingUser.emailAddress,
                password: password
            };
            fetch(`${settings.internalApiUrl}/user/setPassword`, {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json',
                    'authorisation': cred.apiAuth,

                },
                body: JSON.stringify(payload)
            }).then(response => {
                return response.json();
            }).then(jsonResponse => {
                res.status(200);
                return res.json(jsonResponse);
            });
        } else {
            winston.error('Error matching user on auth token');
            res.status(200);
            return res.json({ 'error': error.raise('Invalid User Token.', req.url) });
        }
    }).catch(() => {
        winston.error('Error matching user on auth token lookup');
        res.status(200);
        return res.json({});
    });
});

router.get('/logout', (req, res) => {
    winston.http('inside /auth/logout');
    req.logout();
    winston.http('Redirecting to /');
    return res.redirect('/');
});

module.exports = router;