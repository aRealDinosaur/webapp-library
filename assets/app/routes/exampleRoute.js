const express = require('express');
const router = express.Router();

router.get('/samplePublic', (req, res, next) => {
    if (res.locals.extended) {
        next();
    } else {
        return res.render(`${res.locals.baseTemplate}/samplePublic.ejs`);
    }
});

router.all('*', (req, res, next) => {
    if (req.session.passport && req.session.passport.user) {
        next();
    } else {
        var retUrl = req.originalUrl || '/';
        return res.redirect(`/auth/login?successUrl=${retUrl}`);
    }
});

//PRIVATE ROUTE
router.get('/sampleLocked', (req, res, next) => {
    if (res.locals.extended) {
        next();
    } else {
        console.log('THIS RENDERED IN BASE');
        return res.render(`${res.locals.baseTemplate}/sampleLocked.ejs`);
    }
});

module.exports = router;