const express = require('express');
const router = express.Router();

router.get('/getClientUser', (req, res) => {
    return res.json(res.locals.clientUser);
});

router.get('/getClientUserSession', (req, res) => {
    if (req.session.passport) {
        return res.json(req.session.passport.sessionId);
    } else {
        return res.json({ status: 'anonymous' });
    }
});

module.exports = router;