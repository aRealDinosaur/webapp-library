const express = require('express');
const router = express.Router();
const winston = require('../../shared/winston');

router.all('*', (req, res, next) => {
    //auth route catch all behaviour
    winston.info('In Demo Core Route....');
    next();
});


router.get('/welcome', (req, res, next) => {
    res.locals.demoWelcome = res.locals.demoWelcome || 'You can change this welcome message by extending the route and setting res.locals.demoWelcome';
    res.locals.demoButtons = [
        { buttonText: 'Google', buttonLink: 'https://www.google.com' },
        { buttonText: 'Yahoo', buttonLink: 'https://www.yahoo.co.uk' },
    ];
    if (res.locals.extended) {
        next();
    } else {
        return res.render(`${res.locals.baseTemplate}/demo/welcome`);
    }
});

module.exports = router;