const winston = require('../../shared/winston');
const env = require('../../../credentials/env');
const settings = require('../../../scripts/settings');

module.exports = () => {
    var module = {};

    module.setTemplate = (req, res, next) => {
        winston.info('Setting template parameters...');
        res.locals.baseTemplate = env.baseTemplate || 'base';
        req.app.locals.extendRoutes.forEach((route, i) => {
            req.app.locals.extendRoutes[i] = route.toLowerCase();
        });
        if (req.app.locals.extendRoutes.includes(req.url.split('?')[0].toLowerCase())) {
            res.locals.extended = true;
        } else {
            res.locals.extended = false;
        }

        //set extended Functions
        res.nHRender = (params) => {
            let viewEngine = params.viewEngine || settings.viewEngine || 'react';
            viewEngine = viewEngine.toLowerCase();
            const redirect = params.redirect || false;
            console.log(params);
            switch (viewEngine) {
                case 'react':
                    return res.json(params.payload);
                case 'ejs':
                    if (redirect) {
                        if (params.includeHeader) {
                            return res.redirect(301, params.template);
                        } else {
                            return res.redirect(params.template);
                        }
                    } else {
                        return res.render(params.template);
                    }
                default:
                    next();
            }
        };
        next();
    };
    return module;
};