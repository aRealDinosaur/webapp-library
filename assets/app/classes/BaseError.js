const winston = require('../../shared/winston');

class BaseError {
    constructor(message, params) {
        params = params || {};
        this.message = message || 'an error occured';
        this.logLevel = params.logLevel || 'error';
        this.severity = params.severity || 'normal';
        this.displayType = params.displayType || 'standard';
        this.containerName = params.containerName || 'errorMessage';
        this.modalTarget = params.modalTarget || 'baseErrorModal';
    }

    raise(message, source, params) {
        params = params || {};
        this.message = message || this.message;
        this.timestamp = new Date();
        this.source = source;
        this.logLevel = params.logLevel || this.logLevel;
        this.displayType = params.displayType || this.displayType;
        this.severity = params.severity || this.severity;
        this.containerName = params.containerName || this.containerName;
        this.modalTarget = params.modalTarget || this.modalTarget;
        winston.error(JSON.stringify(this.createErrorObject()));
        return this;
    }

    push(res) {
        res.locals.errors.push(this.createErrorObject());
        res.locals.errorString = JSON.stringify(res.locals.errors);
    }

    createErrorObject() {
        return {
            message: this.message,
            timestamp: this.timestamp,
            source: this.source,
            logLevel: this.logLevel,
            displayType: this.displayType,
            severity: this.severity,
            containerName: this.containerName,
            modalTarget: this.modalTarget
        };
    }
}

module.exports = BaseError;
