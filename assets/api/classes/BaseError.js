const winston = require('../../shared/winston');

class BaseError {
    constructor(message, params) {
        params = params ?? {};
        this.message = message || 'an error occured';
        this.logLevel = params.logLevel || 'error';
        this.displayType = params.displayType || 'standard';
        this.severity = params.severity || 'normal';
    }

    raise(message, source, params) {
        params = params ?? {};
        this.message = message || this.message;
        this.timestamp = new Date();
        this.source = source;
        this.logLevel = params.logLevel || this.logLevel;
        this.displayType = params.displayType || this.displayType;
        this.severity = params.severity || this.severity;
        winston.error(JSON.stringify(this.createErrorObject()));
        return this;
    }

    push(res) {
        res.locals.errors.push(this.createErrorObject());
        res.locals.errorString = JSON.stringify(res.locals.errors);
    }

    createErrorObject() {
        return {
            message: this.message,
            timestamp: this.timestamp,
            source: this.source,
            logLevel: this.logLevel,
            displayType: this.displayType,
            severity: this.severity,
            containerName: this.containerName,
            modalTarget: this.modalTarget
        };
    }

}

module.exports = BaseError;
