const winston = require('../../shared/winston');
const settings = require('../../../scripts/settings');
const cred = require('../../../credentials/credentials');
const nodemailer = require('nodemailer');
const bcrypt = require('bcrypt');

const isEmpty = (obj) => {
    for (var key in obj) {
        // eslint-disable-next-line no-prototype-builtins
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
};

const decodeAuthToken = (authToken) => {
    var asciiString = authToken.split('Basic ')[1] || '';
    asciiString = Buffer.from(asciiString, 'base64').toString('binary');
    const userName = asciiString.split(':')[0] || '';
    const apiKey = asciiString.split(':')[1] || '';
    const accessToken = {
        emailAddress: userName,
        apiKey: apiKey
    };
    return (accessToken);
};

const simpleMail = (to, body, bcc, subject, attachments) => {
    return new Promise((resolve, reject) => {
        const transporter = nodemailer.createTransport({
            host: cred.nodemailer.host,
            port: cred.nodemailer.port,
            secure: cred.nodemailer.secure,
            auth: {
                user: cred.nodemailer.user,
                pass: cred.nodemailer.pass
            }
        });
        const mailoptions = {
            from: {
                name: cred.nodemailer.mailFrom,
                address: cred.nodemailer.fromEmail
            },
            bcc: bcc,
            to: to,
            subject: subject,
            html: body
        };

        if (!isEmpty(attachments)) {
            mailoptions.attachments = attachments;
        }

        transporter.sendMail(mailoptions, (err, info) => {
            if (err) {
                winston.error(`Rejecting sendMail with error: ${err}`);
                reject({ status: 'ERROR', result: err });
            } else {
                winston.debug('Resolving promise with status of "DONE" and variable "info"');
                resolve({ status: 'DONE', result: info });
            }
        });
    });
};

const passwordHasher = (password) => {
    return new Promise((resolve, reject) => {
        return bcrypt.hash(password, settings.bcrypt.saltrounds).then((hash) => {
            winston.debug('password hashed');
            resolve(hash);
        }).catch((err) => {
            reject(err);
        });
    });
};

module.exports = () => {
    var module = {};
    module.decodeAuthToken = decodeAuthToken;
    module.simpleMail = simpleMail;
    module.isEmpty = isEmpty;
    module.passwordHasher = passwordHasher;
    return module;
};
