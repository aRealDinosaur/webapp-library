const express = require('express');
const router = express.Router();

router.post('/welcomeEmail', (req, res) => {
    res.locals.username = req.body.username || '';
    res.locals.tokenLink = req.body.tokenLink || '#';
    res.locals.signature = req.body.signature || '';
    return res.render('emailTemplates/welcomeEmail');
});

router.post('/forgottenPassword', (req, res) => {
    res.locals.username = req.body.username || '';
    res.locals.tokenLink = req.body.tokenLink || '#';
    res.locals.signature = req.body.signature || '';
    return res.render('emailTemplates/forgottenPassword');
});



module.exports = router;