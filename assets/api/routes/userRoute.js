const express = require('express');
const router = express.Router();
const winston = require('../../shared/winston');
const env = require('../../../credentials/env');
const settings = require('../../../scripts/settings');
const authFunctions = require('../scripts/authFunctions')();
const { v4: uuidv4 } = require('uuid');
const fetch = require('node-fetch');
const BaseError = require('../classes/BaseError');
const error = new BaseError('An error occured', { logLevel: 'error', displayType: 'standard', severity: 'normal' });
const databaseFunctions = require('../../shared/databaseFunctions');

//ANONYMOUS ROUTES
router.all('*', (req, res, next) => {
    if (settings.useDatabaseFunctions) {
        next();
    } else {
        error.raise('No Database attatched', req.url, { severity: 'high' });
        return res.json({ error: res.locals.errors });
    }
});

//Check Basic Authentication 
router.all('*', (req, res, next) => {
    let authToken = req.headers.authorisation || '';
    authToken = authFunctions.decodeAuthToken(authToken);
    databaseFunctions.selectTable('users', authToken).then((userRecord) => {
        if (userRecord.length == 1) {
            next();
        } else {
            let test = error.raise('Authentication Failed - Check Your Credentials', req.url);
            console.log(test);
            res.status(401);
            return res.json({ 'error': error.raise('Authentication Failed - Check Your Credentials', req.url) });
        }
    });
});

//ROUTES THAT REQUIRE AUTHENTICATION
//load any matching user records if an emailAddress is supplied in the body and attach it to the response object - should only ever return 1 or 0 records
router.all('*', (req, res, next) => {
    res.locals.matchingUsers = [];
    if (req.body.emailAddress) {
        databaseFunctions.selectTable('users', { emailAddress: req.body.emailAddress }).then((userArray) => {
            res.locals.matchingUsers = userArray;
            next();
        });
    } else {
        next();
    }
});

router.get('/get', (req, res) => {
    return res.json({});
});

router.post('/create', (req, res) => {
    const payLoad = req.body || {};
    if (payLoad.emailAddress) {
        if (res.locals.matchingUsers.length > 0) {
            res.status(400);
            return res.json({ 'error': error.raise('User Already Exists') });
        } else {
            payLoad.userId = uuidv4();
            payLoad.emailVerificationToken = uuidv4();
            databaseFunctions.insertTableRow('users', payLoad).then(() => {
                res.status(200);
                return res.json(payLoad);
            }).catch((err) => {
                error.raise(`Failed To Insert: ${err}`, req.url);
                res.status(400);
                return res.json({ 'error': error('Insert Failed - Check Your Payload') });
            });
        }
    } else {
        res.status(400);
        return res.json({ 'error': error.raise('Invalid Payload', req.url) });
    }
});

router.post('/welcomeEmail', (req, res) => {
    if (res.locals.matchingUsers.length == 1) {
        var payLoad = {};
        var matchingUser = res.locals.matchingUsers[0];
        payLoad.emailVerificationToken = uuidv4();
        databaseFunctions.updateTableRow('users', { userId: matchingUser.userId }, payLoad).then(() => {
            payLoad.username = matchingUser.firstName;
            payLoad.tokenLink = `https://${env.publicBaseUrl}/auth/setPassword?authToken=${payLoad.emailVerificationToken}`;
            payLoad.signature = req.body.signature || '';
            payLoad.subject = req.body.subject || 'Please provide a subject in the payload';
            fetch('http://localhost:3000/render/welcomeEmail', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(payLoad)
            }).then(response => {
                return response.text();
            }).then(textResponse => {

                authFunctions.simpleMail(matchingUser.emailAddress, textResponse, '', payLoad.subject);
                res.status(200);
                return res.json({ message: 'DONE' });
            });
        });
    } else {
        res.status(400);
        return res.json({ 'error': error.raise('Invalid email address', req.url) });
    }
});

router.post('/forgottenPassword', (req, res) => {
    if (res.locals.matchingUsers.length == 1) {
        var payLoad = {};
        var matchingUser = res.locals.matchingUsers[0];
        payLoad.emailVerificationToken = uuidv4();
        databaseFunctions.updateTableRow('users', { userId: matchingUser.userId }, payLoad).then(() => {
            payLoad.username = matchingUser.firstName;
            payLoad.tokenLink = `https://${env.publicBaseUrl}/auth/setPassword?authToken=${payLoad.emailVerificationToken}`;
            payLoad.signature = req.body.signature || '';
            payLoad.subject = req.body.subject || 'Please provide a subject in the payload';

            fetch('http://localhost:3000/render/forgottenPassword', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(payLoad)
            }).then(response => {
                return response.text();
            }).then(textResponse => {
                authFunctions.simpleMail(matchingUser.emailAddress, textResponse, '', payLoad.subject);
                res.status(200);
                return res.json({ message: 'DONE' });
            });
        });
    } else {
        res.status(400);
        return res.json({ 'error': error.raise('Invalid email address', req.url) });
    }
});


router.post('/setPassword', (req, res) => {
    if (res.locals.matchingUsers.length == 1) {
        var matchingUser = res.locals.matchingUsers[0];
        var payLoad = req.body || {};
        if (payLoad.emailAddress && payLoad.password) {
            authFunctions.passwordHasher(payLoad.password).then((hashedPassword) => {
                databaseFunctions.updateTableRow('users', { userId: matchingUser.userId }, { password: hashedPassword, emailVerificationToken: '' }).then(() => {
                    res.status(200);
                    return res.json({ message: 'DONE' });
                }).catch((err) => {
                    winston.error(err);
                    res.status(400);
                    return res.json({ 'error': error.raise('An unknown error occured', req.url) });
                });
            });
        } else {
            res.status(400);
            return res.json({ 'error': error.raise('Invalid payload', req.url) });
        }
    } else {
        res.status(400);
        return res.json({ 'error': error.raise('Invalid email address', req.url) });
    }
});

module.exports = router;
